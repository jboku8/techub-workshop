const mongoose = require('mongoose');

const TodoSchema = new mongoose.Schema({
    message: {
        type: String,
        required: true,
        unique: true
    },
    completed: {
        type: Boolean,
        default: false,
        required: true
    },
    dueOn: Date
}, { timestamps: true})

module.exports = mongoose.model('todo', TodoSchema);