const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const UserSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    betaUser: {
        type: Boolean,
        default: true
    },
    birthDay: Date,
    password: {
        type: String,
        required: true
    },
    avatar: {
        type: String,
    },
    gender: {
        type: String,
        required: true,
        enum:["male", "female", "other"]
    },
    address: {
        other: String,
        street: Number,
        city: String
    }
})
// user.address.city

// UserSchema.pre('save', (next) => {

// })

UserSchema.methods.checkPassword = (password) => {
    const passwordHash = this.password;

    return new Promise((resolve, reject) => {
        bcrypt.compare(password, passwordHash, (error, result) => {
            if(error) {
                reject(error);
            }else {
                resolve(result);
            }
        });
    });
}

module.exports = mongoose.model('user', UserSchema);