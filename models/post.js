const mongoose = require('mongoose');

const PostSchema = new mongoose.Schema({
    title : {
        type : String
    },
    body : {
        type : String
    },
    user : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'user',
        required : true
    },
    comments : {
        author : {
            type : mongoose.Schema.Types.ObjectId,
            ref : 'user',
            required : true
        },
        body : {
            type : String
        }
    },
    point : {
        type: Number,
        default : 0
    },
    date : {
        default : Date.now()
    }
},
{ timestamps : true})


module.exports = mongoose.model('post',  PostSchema);