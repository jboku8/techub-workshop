const User = require('./models/user');
const jwt = require('jsonwebtoken');
const crud = require('./cruds/userCruds');

const secret = 'mySecret';

const newToken = (user) => {
    return jwt.sign({ id: user.id }, secret, { expiresIn: 300000});
}

const verifyToken = (token) => {
    return new Promise((resolve, reject) => {
        jwt.verify(token, secret, (error, payload) => {
            if(error) {
                reject(error);
            }else {
                resolve(payload);
            }
        });
    });
}

const protect = async (req, res, next) => {
    // "Authorization" : "Bearer rgegrerhd4g8es48s4g89s4g89s4g9s4g";
    const bearer = req.headers.authorization;
    if(!bearer || !bearer.startsWith('Bearer')) {
        return res.status(400).end();
    } 

    const token = bearer.split('Bearer')[1];
    const payload = undefined;
    
    try {
        payload = await verifyToken(token);

    }
    catch(err) {
        return res.status(400).send(err);
    }
    
    const user = await User.findById(payload.id).select('-password').lean().exec();
    
    if(!user) {
        return res.status(400).end();
    }

    req.user = user;
    next();
}

const signIn = async (req, res) => {
    if(!req.body.email || !req.body.password) {
        return res.status(400).send({message: "fail"});
    }

    try {
        const user = await User.findOne({ email: req.body.email}).select('email password').exec();
        if(!user) {
            return res.status(400).send({message: "fail"});
        }

        const match = await user.checkPassword(req.body.password);

        if(!match) {
            return res.status(400).send({message: "fail"});
        }

        const newToken = newToken(user);

        return res.status(200).send({token: newToken});
    }
    catch(err) {
        return res.status(400).send(err);
    }
}

const signUp = async (req, res) => {

}

module.exports = {
    newToken, 
    verifyToken,
    protect
}