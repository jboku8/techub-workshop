const express = require('express');
const { json, urlencoded } = require('express');
const morgan = require('morgan');
const connect = require('./connect');
const url = 'mongodb://admin:admin123@ds127995.mlab.com:27995/natia_db';
const Todo = require('./models/todo');
const expressLayout = require('express-ejs-layouts');
const auth = require('./auth');

const app = express();
const userController = require("./controllers/users.controller");

app.use(expressLayout);
app.use(express.static("./public"));
app.use( morgan('dev') );
app.use( json())
app.use( urlencoded({extended:true}));
app.set("views", "./views");
app.set("view engine", "ejs");

app.use('/user', auth.protect);
app.use("/user", userController);


app.get('/todo/:id', async (req, res) => {
    const todoId = req.params.id;
    const todo = await Todo.findById(todoId).exec();
    // _id, id
    res.status(200).json(todo);
});

app.get('/login', (req,res) => {
    res.render('login');
})

app.post('/todos', async (req, res) => {
    const newTodo = req.body.todo;
    try {
        const todo = await Todo.create({message: newTodo});
        res.status(201).json({message:""});
    }
    catch( err ) {
        console.error(err)
        res.status(401).json(err);
    }
});

connect(url)
.then( () => app.listen(8000, () => {
    console.log(`server on port http://localhost:8000`);
}))
.catch( err => console.error(err))
