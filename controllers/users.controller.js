
const {Router} = require("express");
const cruds = require('../cruds/userCruds');
const bcrypt = require('bcrypt');
const router = new Router();
const saltRounds = 15;
router.get("/", (req,res) => {
    res.render("register")
});

router.post('/register', async (req,res) =>{
    
    try{
        let errors = [];
        
        if(req.body.password.length < 6){
            errors.push({'msg' : 'მინიმუმ 6 სიმბოლო'})
        }
        if(req.body.password != req.body.repassword){
            errors.push({'msg' : 'პაროლები არ ემთხვევა'})
        }

        if(errors.length > 0 ){
            return res.redirect('/register')
        }

        bcrypt.hash(req.body.password, saltRounds).then(async function(hash) {
            const userDetails = {...req.body};
            userDetails.password = hash;
            // Store hash in your password DB.
            const newUser = await cruds.createUser(userDetails);
            res.redirect('/login');
        });
    }
    catch(err ) {
        console.error(err);
        res.status(401).json({status: "fail"})
    }
})
module.exports = router;